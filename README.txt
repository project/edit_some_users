Edit some users
===============

By default, Drupal defines the "administer users" permission, which allows users of a given role to edit all users.

This module provides developers with a hook they can implement to allow certain users (which normally don't have the "administer users" permission) to edit particular users.

    /**
     * Check whether a source account should be able to edit a target account.
     */
    function hook_edit_some_users_check($source, $target) {
      // if your module determines that $source should be allowed to edit
      // $target, return TRUE here.
      return FALSE;
    }

This module does not provide a mechanism to remove editing rights from users who already have them; it can only add editing rights to users who normally don't have them.

*This module is for developers and does not provide a graphical user interface. You need to know how to implement hooks in order to gain any advantage from this module.*

Example #1
----------

If you want administrators to be able to edit authenticated users but not other administrators, you could remove administrators' "administer users" permissions and implement the hook as is shown in the `edit_some_users_example` module.

Example #2
----------

Your site defines organizations, which have representatives. Authenticated users are part of organizations. If you want organization representatives to be able to edit only those users who are part of the organization, you would make sure representatives do not, by default, have the "administer users" permission; then, implement the `hook_edit_some_users_check()` hook and return TRUE if the source is a representative for an organization that target is a member of.

_Sponsored by [CGI](http://cgi.com/)_