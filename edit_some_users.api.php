<?php

/**
 * @file
 * API documentation for edit_some_users.
 */

/**
 * Check whether a source account should be able to edit a target account.
 *
 * If source can edit target, returning FALSE will not remove that permission.
 * However if source cannot edit target, and an implementor of this hook returns
 * TRUE, then target will have the permission to edit target. The reason this
 * module does not remove permissions is because it might pose a security risk:
 * if this module is disabled (for an update for example, or any other reason),
 * then users would have privileges they are not supposed to have. As it stands,
 * when this module is disabled, users lose privileges, which is not a security
 * issue.
 *
 * @param $source
 *   A Drupal user object which wants to edit target.
 * @param $target
 *   A Drupal user object which we want to know if source can edit.
 *
 * @return
 *   TRUE|FALSE
 */
function hook_edit_some_users_check($source, $target) {
  // if your module determines that $source should be allowed to edit
  // $target, return TRUE here.
  return FALSE;
}
